This project is an implementation of hasher using two types of hash function: crc32 and sum algorithms.

# make build directory
mkdir build
cd build

# generate projects
cmake ..

# build exe and unittests
cmake --build .
```
Now you can use the hasher.
Next, follow this steps:

1. choose hasher type crc32 or sum

2. choose filename, where the filename is the name of the file you want to get hash of, or path to such file

3. now you can use the following commands

./hasher -m crc32 filename        or
./hasher --mode crc32 filename

or with another type of hasher:

./hasher -m sum filename          or
./hasher --mode sum filename

4. if you have any problems, you can get the "help"
./hasher -h          or
./hasher --help


