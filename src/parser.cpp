#include <boost/program_options.hpp>
#include "parser.h"
#include <iostream>

namespace po = boost::program_options;

using std::endl;

const std::string& Parser::getHasherType() {
    return hasherType;
}

const std::string& Parser::getFileName() {
    return fileName;
}

const po::options_description& Parser::getDescription() {
    return desc;
}

const po::variables_map& Parser::getVariablesMap() {
    return vm;
}

Parser::Parser(int argc, char** argv) {
    desc.add_options()
        ("help,h", "shows help")
        ("mode,m", po::value(&hasherType), "<<crc32> or <sum>> and <filename>")
    ;

    po::positional_options_description p;
    p.add("input-file", -1);

    po::options_description hidden("Hidden options");
    hidden.add_options()
        ("input-file", po::value(&fileName), "input file")
    ;

    po::options_description cmdline_options;
    cmdline_options.add(desc).add(hidden);

    try {
        po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
        po::notify(vm);
    }
    catch(po::error& e) {
        std::cout << e.what() << endl;
    }
};
