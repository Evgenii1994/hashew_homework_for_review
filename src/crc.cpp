#include <stdint.h>
#include "crc.h"

Crc32::Crc32() {
    for (int i = 0; i < 256; i++) {
        crc = i;
        for (int j = 0; j < 8; j++)
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;

        crc_table[i] = crc;
    }

    crc = 0xFFFFFFFFUL;
}

uint32_t Crc32::getResult() {
      return crc;
}

void Crc32::ProcessCRC(const char* data, int len) {
    if (len == 0) {
        crc = crc ^ 0xFFFFFFFFUL;
    }
    while (len--) {
        crc = crc_table[(crc ^ *data++) & 0xFF] ^ (crc >> 8);
    }
}
