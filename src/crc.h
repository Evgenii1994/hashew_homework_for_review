#include <stdint.h>

class Crc32 {
    uint32_t crc_table[256];
    uint32_t crc;
public:
    uint32_t getResult();
    Crc32();
    void ProcessCRC(const char* pData, int nLen);
};
