#include <iostream>
#include <fstream>
#include <stdint.h>
#include <boost/program_options.hpp>
#include "crc.cpp"
#include "sum.cpp"
#include "parser.cpp"

namespace po = boost::program_options;

using std::cout;
using std::vector;
using std::cerr;
using std::string;
using std::endl;
using std::ifstream;

int main(int argc, char** argv) {

    Parser obj(argc, argv);

    if (obj.getVariablesMap().count("help")) {
        cout << endl << obj.getDescription() << endl;
        return 0;
    }

    if (obj.getVariablesMap().count("mode")) {
        if (obj.getHasherType() != "crc32" && obj.getHasherType() != "sum"){
            cerr << "Wrong hasher type: " << obj.getHasherType() << endl;
            cout << endl << obj.getDescription() << endl;
            return 1;
        }
    }

    if (obj.getVariablesMap().count("mode") && obj.getVariablesMap().count("input-file")) {
        const char *cstr = obj.getFileName().c_str();
        ifstream ifs(cstr, ifstream::binary);

        if (!ifs)
        {
            cerr << "ERROR: cannot open " << obj.getFileName() << " file" << endl;
            cout << endl << obj.getDescription() << endl;
            return 1;
        }

        if (obj.getHasherType() == "crc32") {
            const unsigned int BUFSIZE = 32;
            std::vector<char> buffer( BUFSIZE );
            Crc32 objct;
            while(true) {
                ifs.read(&buffer[0], BUFSIZE);
                const unsigned n = ifs.gcount();
                objct.ProcessCRC(&buffer[0], n);
                if (!n) {
                  break;
                }
            }
            cout << "Result: " << objct.getResult() << endl;
            return 0;
        }

        if (obj.getHasherType() == "sum") {
            std::uint32_t sum32 = 0;
            const unsigned int BUFSIZE = 4;
            std::vector<char> buffer( BUFSIZE );
            while (true) {
              ifs.read(&buffer[0], BUFSIZE);
              const unsigned n = ifs.gcount();
              sum32 += convert(&buffer[0], n);
              if (!n) {
                break;
              }
            }
            cout << "Result: " << sum32 << endl;
            return 0;
        }
    }

    cout << endl << obj.getDescription() << endl;

    return 1;
}
