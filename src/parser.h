#include <boost/program_options.hpp>

class Parser {
    std::string hasherType;
    std::string fileName;
    boost::program_options::options_description desc;
    boost::program_options::variables_map vm;
public:
    const std::string& getHasherType();
    const std::string& getFileName();
    const boost::program_options::options_description& getDescription();
    const boost::program_options::variables_map& getVariablesMap();
    Parser(int argc, char** argv);
};
