#include <stdint.h>
#include "sum.h"

uint32_t convert(const char* pData, int nLen) {
uint32_t res = 0;
  for (int i = 0; i < nLen; ++i) {
    res += static_cast<uint32_t>(pData[i]) << (i * 8);
  }
  return res;
}
