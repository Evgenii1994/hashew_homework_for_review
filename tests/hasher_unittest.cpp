#include "gtest/gtest.h"
#include "crc.h"
#include "sum.h"
#include "parser.h"
#include <vector>
#include <stdint.h>

using std::vector;

TEST(HasherTest, ParseHelp1)
{
    int argc = 2;
    char ** argt = new char*[2];
    char nameOfProgramm[] = "hasher";
    char typeOfArgument[] = "-h";
    argt[0] = nameOfProgramm;
    argt[1] = typeOfArgument;
    Parser obj(argc, argt);

    bool res = obj.getVariablesMap().count("help");
    ASSERT_EQ(1, res);

}

TEST(HasherTest, ParseHelp2)
{
    int argc = 2;
    char ** argt = new char*[2];
    char nameOfProgramm[] = "hasher";
    char typeOfArgument[] = "--help";
    argt[0] = nameOfProgramm;
    argt[1] = typeOfArgument;
    Parser obj(argc, argt);

    bool res = obj.getVariablesMap().count("help");
    ASSERT_EQ(1, res);
}

TEST(HasherTest, ParseMode1)
{
    int argc = 3;
    char ** argt = new char*[3];
    char nameOfProgramm[] = "hasher";
    char typeOfArgument[] = "-m";
    char argument[] = "crc32";
    argt[0] = nameOfProgramm;
    argt[1] = typeOfArgument;
    argt[2] = argument;
    Parser obj(argc, argt);

    bool res = obj.getVariablesMap().count("mode");
    ASSERT_EQ(1, res);
}

TEST(HasherTest, ParseMode2)
{
    int argc = 3;
    char ** argt = new char*[3];
    char nameOfProgramm[] = "hasher";
    char typeOfArgument[] = "--mode";
    char argument[] = "sum";
    argt[0] = nameOfProgramm;
    argt[1] = typeOfArgument;
    argt[2] = argument;
    Parser obj(argc, argt);

    bool res = obj.getVariablesMap().count("mode");
    ASSERT_EQ(1, res);
}

TEST(HasherTest, ParseModeAndFile)
{
    int argc = 4;
    char ** argt = new char*[4];
    char nameOfProgramm[] = "hasher";
    char typeOfArgument[] = "--mode";
    char argument[] = "crc32";
    char fileName[] = "abc";
    argt[0] = nameOfProgramm;
    argt[1] = typeOfArgument;
    argt[2] = argument;
    argt[3] = fileName;
    Parser obj(argc, argt);

    bool res = obj.getVariablesMap().count("mode") && obj.getVariablesMap().count("input-file");
    ASSERT_EQ(1, res);
}

TEST(HasherTest, Crc32Test1)
{
    Crc32 obj;
    vector<char> buffer = {'a', 'b'};
    obj.ProcessCRC(&buffer[0], 2);
    obj.ProcessCRC(&buffer[0], 0);

    ASSERT_EQ(2659403885, obj.getResult());
}

TEST(HasherTest, Crc32Test2)
{
    Crc32 obj;
    vector<char> buffer = {'c', 'd', 'e', 'f', 'g'};
    obj.ProcessCRC(&buffer[0], 5);
    obj.ProcessCRC(&buffer[0], 0);

    ASSERT_EQ(43625413, obj.getResult());
}

TEST(HasherTest, Crc32Test3)
{
    Crc32 obj;
    vector<char> buffer = {};
    obj.ProcessCRC(&buffer[0], 0);

    ASSERT_EQ(0, obj.getResult());
}

TEST(HasherTest, sumTest1)
{
    uint32_t sum32 = 0;
    vector<char> buffer = {'a', 'a'};
    sum32 += convert(&buffer[0], 2);
    ASSERT_EQ(24929, sum32);
}

TEST(HasherTest, sumTest2)
{
    uint32_t sum32 = 0;
    vector<char> buffer = {'a', 'a', 'a', 'a', 'a'};
    sum32 += convert(&buffer[0], 4);
    sum32 += convert(&buffer[0], 1);
    ASSERT_EQ(1633771970, sum32);
}
